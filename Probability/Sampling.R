# ------------------------------------------------------------------------------------------------
# Code: Permutations and combination
permutations(5,2)    # ways to choose 2 numbers in order from 1:5
all_phone_numbers <- permutations(10, 7, v = 0:9)
(n <- nrow(all_phone_numbers))
(index <- sample(n, 5))
all_phone_numbers[index,]




# ------------------------------------------------------------------------------------------------
# Two teams, A and B , are playing a seven series game series. Team A
# is better than team B and has a p>0.5 chance of winning each game.
# Let's assign the variable 'p' as the vector of probabilities that team A will win.
p <- seq(0.5, 0.95, 0.025)
# Given a value 'p', the probability of winning the series for the underdog team B can be computed with the following function based on a Monte Carlo simulation:
prob_win <- function(p){
  B <- 10000
  result <- replicate(B, {
    b_win <- sample(c(1,0), 7, replace = TRUE, prob = c(1-p, p))
    sum(b_win)>=4
  })
  mean(result)
}
# Apply the 'prob_win' function across the vector of probabilities that team A will win to determine the probability that team B will win. Call this object 'Pr'.
Pr<-sapply(p,prob_win)
# Plot the probability 'p' on the x-axis and 'Pr' on the y-axis.
plot(p, Pr)
# -------------------------------------------------------------------------------------------------------
cyan <- 3
magenta <- 5
yellow <- 7
# The variable `p_1` is the probability of choosing a cyan ball from the box on the first draw.
p_1 <- cyan / (cyan + magenta + yellow)
# Assign a variable `p_2` as the probability of not choosing a cyan ball on the second draw without replacement.
p_2 <- (magenta + yellow)/(cyan + magenta + yellow-1)
# Calculate the probability that the first draw is cyan and the second draw is not cyan using `p_1` and `p_2`.
(p_1*p_2)

# -------------------------------------------------------------------------------------------------------
# Assign the variable `p_cavs_win4` as the probability that the Cavs will win the first four games of the series.
p_cavs_win4<-0.60^4
# Using the variable `p_cavs_win4`, calculate the probability that the Celtics win at least one game in the first four games of the series.
1-p_cavs_win4


# -------------------------------------------------------------------------------------------------------
# Assign a variable 'n' as the number of remaining games.
n <- 6
# Assign a variable `outcomes` as a vector of possible game outcomes, where 0 indicates a loss and 1 indicates a win for the Cavs.
outcomes <- c(0,1)
# Assign a variable `l` to a list of all possible outcomes in all remaining games. Use the `rep` function on `list(outcomes)` to create list of length `n`.
l <- rep(list(outcomes),n)
# Create a data frame named 'possibilities' that contains all combinations of possible outcomes for the remaining games.
possibilities <- expand.grid(l)
# Create a vector named 'results' that indicates whether each row in the data frame 'possibilities' contains enough wins for the Cavs to win the series.
results<-rowSums(possibilities)>=4
# Calculate the proportion of 'results' in which the Cavs win the series. Print the outcome to the console.
mean(results)

# -------------------------------------------------------------------------------------------------------
# This line of example code simulates four independent random games where the Celtics either lose or win. Copy this example code to use within the `replicate` function.
simulated_games <- sample(c("lose","win"), 4, replace = TRUE, prob = c(0.6, 0.4))
# The variable 'B' specifies the number of times we want the simulation to run. Let's run the Monte Carlo simulation 10,000 times.
B <- 10000
# Use the `set.seed` function to make sure your answer matches the expected result after random sampling.
set.seed(1)
# Create an object called `celtic_wins` that replicates two steps for B iterations: 
## (1) generating a random four-game series `simulated_games` using the example code, then 
## (2) determining whether the simulated series contains at least one win for the Celtics.
celtic_wins<-replicate(B,any(sample(c("lose","win"), 4, replace = TRUE, prob = c(0.6, 0.4))=="win"))
# Calculate the frequency out of B iterations that the Celtics won at least one game. Print your answer to the console.
print(mean(celtic_wins))


# -------------------------------------------------------------------------------------------------------
B <- 10000
tallest <- replicate(B, {
  simulated_data <- rnorm(800, m, s)
  max(simulated_data)
})

# -------------------------------------------------------------------------------------------------------
# The variable `B` specifies the number of times we want the simulation to run. Let's run the Monte Carlo simulation 10,000 times.
B <- 10000
# Use the `set.seed` function to make sure your answer matches the expected result after random sampling.
set.seed(1)
# Create an object called `results` that replicates for `B` iterations a simulated series and determines whether that series contains at least four wins for the Cavs.
results<-replicate(B,sum(sample(c(0,1), 6, replace = TRUE))>=4)
# Calculate the frequency out of `B` iterations that the Cavs won at least four games in the remainder of the series. Print your answer to the console.
print(mean(results))


# -------------------------------------------------------------------------------------------------------
# Assign a variable 'female_avg' as the average female height.
female_avg <- 64
# Assign a variable 'female_sd' as the standard deviation for female heights.
female_sd <- 3
# Using variables 'female_avg' and 'female_sd', calculate the probability that a randomly selected female is shorter than 5 feet. Print this value to the console.
pnorm(5*12,female_avg,female_sd)
# Using variables 'female_avg' and 'female_sd', calculate the probability that a randomly selected female is 6 feet or taller. Print this value to the console.
1-pnorm(6*12,female_avg,female_sd)
# Using variables 'female_avg' and 'female_sd', calculate the probability that a randomly selected female is between the desired height range. Print this value to the console.
pnorm(67,female_avg,female_sd)-pnorm(61,female_avg,female_sd)
# To a variable named 'taller', assign the value of a height that is one SD taller than average.
taller<-female_avg+female_sd
# To a variable named 'shorter', assign the value of a height that is one SD shorter than average.
shorter<-female_avg-female_sd
# Calculate the probability that a randomly selected female is between the desired height range. Print this value to the console.
pnorm(taller,female_avg,female_sd)-pnorm(shorter,female_avg,female_sd)


# -------------------------------------------------------------------------------------------------------
# Assign a variable 'male_avg' as the average male height.
male_avg <- 69
# Assign a variable 'male_sd' as the standard deviation for male heights.
male_sd <- 3
# Determine the height of a man in the 99th percentile of the distribution.
qnorm(0.99,male_avg,male_sd)

# -------------------------------------------------------------------------------------------------------
# Create an object called `highestIQ` that contains the highest IQ score from each random distribution of 10,000 people.
B <- 1000
set.seed(1)
highestIQ <- replicate(B, {
  simulated_data <- rnorm(10000,100,15)
  max(simulated_data)
})



