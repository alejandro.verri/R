# ====
# A restaurant manager wants to advertise that his lunch special offers enough choices to eat different meals every day of the year. 
#He doesn't think his current special actually allows that number of choices, but wants to change his special if needed to allow 
#at least 365 choices.
#A meal at the restaurant includes 1 entree, 2 sides, and 1 drink. He currently offers  
# a choice of1 entree from a list of 6 options, 
# a choice of 2 different sides from a list of 6 options, 
#  and a choice of 1 drink from a list of 2 options.

# How many meal combinations are possible with the current menu?
ENTREES<-paste("C",c(1:6),sep="")
SIDE<-paste("S",c(1:6),sep="")
SIDES<-combinations(6,2,v=SIDE) #not permutations. orden does not matter
SIDES<-sapply(1:nrow(SIDES),function(r) paste(SIDES[r,1],SIDES[r,2]))
DRINKS<-paste("D",c(1:2),sep="")
MENU<-expand.grid(entree=ENTREES,sides=SIDES,drink=DRINKS)
nrow(MENU)

#How many combinations are possible if he expands his original special to 3 drink options?
DRINKS<-paste("D",c(1:3),sep="")
MENU<-expand.grid(entree=ENTREES,sides=SIDES,drink=DRINKS)
nrow(MENU)

# How many meal combinations are there if customers can choose from 
# 6 entrees, 3 drinks, and select 3 sides from the current 6 options?

SIDES<-combinations(6,3,v=SIDE) #not permutations. orden does not matter
SIDES<-sapply(1:nrow(SIDES),function(r) paste(SIDES[r,1],SIDES[r,2]))
DRINKS<-paste("D",c(1:3),sep="")
MENU<-expand.grid(entree=ENTREES,sides=SIDES,drink=DRINKS)
nrow(MENU)

entreeChoices<-function(n) {
  ENTREES<-paste("C",c(1:n),sep="")
  SIDE<-paste("S",c(1:6),sep="")
  SIDES<-combinations(6,2,v=SIDE) #not permutations. orden does not matter
  SIDES<-sapply(1:nrow(SIDES),function(r) paste(SIDES[r,1],SIDES[r,2]))
  DRINKS<-paste("D",c(1:3),sep="")
  MENU<-expand.grid(entree=ENTREES,sides=SIDES,drink=DRINKS)
  nrow(MENU)
}


sideChoices<-function(m) {
  ENTREES<-paste("C",c(1:6),sep="")
  SIDE<-paste("S",c(1:m),sep="")
  SIDES<-combinations(m,2,v=SIDE) #not permutations. orden does not matter
  SIDES<-sapply(1:nrow(SIDES),function(r) paste(SIDES[r,1],SIDES[r,2]))
  DRINKS<-paste("D",c(1:3),sep="")
  MENU<-expand.grid(entree=ENTREES,sides=SIDES,drink=DRINKS)
  nrow(MENU)
}